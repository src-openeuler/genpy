# genpy

#### 介绍
genpy 是python相关的ROS的message和服务生成器

#### 软件架构
软件架构说明

genpy 包含python相关的ROS的message和服务生成器

文件内容:
```
genpy/
├── CHANGELOG.rst
├── cmake
│   └── genpy-extras.cmake.em
├── CMakeLists.txt
├── doc
│   ├── conf.py
│   ├── index.rst
│   └── Makefile
├── package.xml
├── README.md
├── rosdoc.yaml
├── scripts
│   ├── CMakeLists.txt
│   ├── genmsg_py.py
│   └── gensrv_py.py
├── setup.py
├── src
│   └── genpy
└── test
    ├── files
    ├── __init__.py
    ├── msg
    ├── test_genpy_base.py
    ├── test_genpy_dynamic.py
    ├── test_genpy_generate_numpy.py
    ├── test_genpy_generate_struct.py
    ├── test_genpy_generator.py
    ├── test_genpy_message.py
    ├── test_genpy_python_safe.py
    ├── test_genpy_rostime.py
    └── test_genpy_rostime_truediv.py
```

#### 安装教程

1.  下载rpm包

aarch64:

wget https://117.78.1.88/build/home:davidhan:branches:openEuler:22.03:LTS/standard_aarch64/aarch64/ros-noetic-ros-genpy/ros-noetic-ros-genpy-0.6.15-2.oe2203.aarch64.rpm

x86_64:

wget https://117.78.1.88/build/home:davidhan:branches:openEuler:22.03:LTS/standard_x86_64/x86_64/ros-noetic-ros-genpy/ros-noetic-ros-genpy-0.6.15-2.oe2203.x86_64.rpm 

2. 安装rpm包

aarch64:

sudo rpm -ivh ros-noetic-ros-genpy-0.6.15-2.oe2203.aarch64.rpm --nodeps --force

x86_64:

sudo rpm -ivh ros-noetic-ros-genpy-0.6.15-2.oe2203.x86_64.rpm --nodeps --force


#### 使用说明

依赖环境安装:

sh /opt/ros/noetic/install_dependence.sh

安装完成以后，在/opt/ros/noetic/目录下有如下输出,则表示安装成功

输出:
```
genpy/
├── cmake.lock
├── env.sh
├── etc
│   └── ros
│       └── genmsg
├── lib
│   ├── genpy
│   │   ├── cmake.lock
│   │   ├── genmsg_py.py
│   │   └── gensrv_py.py
│   ├── pkgconfig
│   │   └── genpy.pc
│   └── python2.7
│       └── dist-packages
├── local_setup.bash
├── local_setup.sh
├── local_setup.zsh
├── setup.bash
├── setup.sh
├── _setup_util.py
├── setup.zsh
└── share
    └── genpy
        └── cmake
```

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
