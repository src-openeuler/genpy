# genpy

#### Description
genpy package thath Python ROS message and service generators.

#### Software Architecture
Software architecture description

genpy include Python ROS message and service generators.

input:
```
genpy/
├── CHANGELOG.rst
├── cmake
│   └── genpy-extras.cmake.em
├── CMakeLists.txt
├── doc
│   ├── conf.py
│   ├── index.rst
│   └── Makefile
├── package.xml
├── README.md
├── rosdoc.yaml
├── scripts
│   ├── CMakeLists.txt
│   ├── genmsg_py.py
│   └── gensrv_py.py
├── setup.py
├── src
│   └── genpy
└── test
    ├── files
    ├── __init__.py
    ├── msg
    ├── test_genpy_base.py
    ├── test_genpy_dynamic.py
    ├── test_genpy_generate_numpy.py
    ├── test_genpy_generate_struct.py
    ├── test_genpy_generator.py
    ├── test_genpy_message.py
    ├── test_genpy_python_safe.py
    ├── test_genpy_rostime.py
    └── test_genpy_rostime_truediv.py

```

#### Installation

1.  Download RPM

aarch64:

wget https://117.78.1.88/build/home:davidhan:branches:openEuler:22.03:LTS/standard_aarch64/aarch64/ros-noetic-ros-genpy/ros-noetic-ros-genpy-0.6.15-2.oe2203.aarch64.rpm

x86_64:

wget https://117.78.1.88/build/home:davidhan:branches:openEuler:22.03:LTS/standard_x86_64/x86_64/ros-noetic-ros-genpy/ros-noetic-ros-genpy-0.6.15-2.oe2203.x86_64.rpm 

2.  Install RPM

aarch64:

sudo rpm -ivh ros-noetic-ros-genpy-0.6.15-2.oe2203.aarch64.rpm --nodeps --force

x86_64:

sudo rpm -ivh ros-noetic-ros-genpy-0.6.15-2.oe2203.x86_64.rpm --nodeps --force

#### Instructions

Dependence installation

sh /opt/ros/noetic/install_dependence.sh

Exit the following output file under the /opt/ros/noetic/ directory , Prove that the software installation is successful

output:
```
genpy/
├── cmake.lock
├── env.sh
├── etc
│   └── ros
│       └── genmsg
├── lib
│   ├── genpy
│   │   ├── cmake.lock
│   │   ├── genmsg_py.py
│   │   └── gensrv_py.py
│   ├── pkgconfig
│   │   └── genpy.pc
│   └── python2.7
│       └── dist-packages
├── local_setup.bash
├── local_setup.sh
├── local_setup.zsh
├── setup.bash
├── setup.sh
├── _setup_util.py
├── setup.zsh
└── share
    └── genpy
        └── cmake

```

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
